<%@ page import="prueba1.File" %>



<div class="fieldcontain ${hasErrors(bean: fileInstance, field: 'fileType', 'error')} required">
	<label for="fileType">
		<g:message code="file.fileType.label" default="File Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fileType" pattern="${fileInstance.constraints.fileType.matches}" required="" value="${fileInstance?.fileType}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: fileInstance, field: 'content', 'error')} required">
	<label for="content">
		<g:message code="file.content.label" default="Content" />
		<span class="required-indicator">*</span>
	</label>
	<input type="file" id="content" name="content" />

</div>

<div class="fieldcontain ${hasErrors(bean: fileInstance, field: 'post', 'error')} ">
	<label for="post">
		<g:message code="file.post.label" default="Post" />
		
	</label>
	<g:select name="post" from="${prueba1.Post.list()}" multiple="multiple" optionKey="id" size="5" value="${fileInstance?.post*.id}" class="many-to-many"/>

</div>

