package prueba1

class User {

    String name
	String lastName
	int age
	String userName
	String password
	
	static constraints = {
		name blank: false, nullable: false, size:3..50
		lastName blank: false, nullable: false, size:3..50
		age blank: false, nullable: false, min: 13
		password blank: false, nullable: false, matches: "([a-zA-Z0-9]*[A-Z]+[a-zA-Z0-9]*[0-9]+[a-zA-Z0-9]*)|([a-zA-Z0-9]*[0-9]+[a-zA-Z0-9]*[A-Z]+[a-zA-Z0-9]*)", minSize: 8
		userName blank: false, nullable: false, unique:true
	}
}
