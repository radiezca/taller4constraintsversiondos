package prueba1

class Admin extends User{
	int level
	double rating

	static hasMany = [forum:Forum]

	static constraints = {
		level nullable: false, range: 1..5
		rating nullable: false, range: 0..100
	}
}
