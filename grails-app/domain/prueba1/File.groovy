package prueba1

class File {

    String fileType
	Byte[]	content
	//long	 size

	static hasMany = [post:Post]

	static constraints = {
		fileType blank:false, nullable:false, matches:"[a-zA-Z]*/[a-zA-Z]*"
		content blank:false, nullable:false
		//size blank:false, nullable:false, max: 1310720
	}
	
	def download = {}
	def share = {}
}