package prueba1

import java.util.Date;

class Forum {

    String name
	Date dateCreated
	int category

	static belongsTo = [admin:Admin] 
	static hasMany = [post:Post]
	/* debido a que en Post existe la relaci�n belongsTo los 
		post se borran al tiempo que se borra un foro*/ 

	static constraints = {
		name nullable: false, size: 3..20, unique: true
		category nullable: false, range:3..15
	}
	//Antes de insertar guarda la fecha de creaci�n
	def beforeInsert() {
	    dateCreated = new Date()
	}
  
}
