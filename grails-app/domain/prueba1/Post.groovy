package prueba1

import java.util.Date;

class Post {

    String topic
	Date dateCreated
	Date lastUpdate
	boolean	itsAllowed

	static belongsTo = [regular:Regular,forum:Forum]
	static hasONe = [file:File]

	static constraints = {
		topic nullable:false, size:3..50
		dateCreated nullable:false, min: new Date()
		lastUpdate nullable:false, min: new Date()
		itsAllowed nullable:false
	}
	
	def comment = {}
	
	def rate = {}
	
	def share = {}
	
	//Esto se justifica por la misma raz�n de la clase Forum
	def beforeInsert() {
		dateCreated = new Date()
	}
	
}
