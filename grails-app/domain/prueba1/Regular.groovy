package prueba1

class Regular extends User{

    int postViews
	int strikesNumber
	int starsNumber

	static hasMany = [post:Post]
	
	static constraints = {
		postViews nullable:false, defaultValue: 0
		strikesNumber nullable:false, range: 0..3
		starsNumber nullable:false, range: 0..5
	}
}
